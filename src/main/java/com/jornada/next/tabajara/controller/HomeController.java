package com.jornada.next.tabajara.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @GetMapping("/")
    public String getHome(){
        return "Bem vindo ao Banco Digital Tabajara";
    }
}
