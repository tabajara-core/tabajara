package com.jornada.next.tabajara.repository;

import com.jornada.next.tabajara.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {

    @Override
    List<Cliente> findAll();
}