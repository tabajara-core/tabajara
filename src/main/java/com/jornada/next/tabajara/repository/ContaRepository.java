package com.jornada.next.tabajara.repository;

import com.jornada.next.tabajara.model.Conta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContaRepository extends JpaRepository<Conta, Long> {

    @Override
    List<Conta> findAll();
}