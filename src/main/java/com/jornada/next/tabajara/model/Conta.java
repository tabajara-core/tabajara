package com.jornada.next.tabajara.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Conta implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String agencia;
    private String conta;
    private String dac;

    public Conta() {
    }

    public Conta(String agencia, String conta, String dac) {
        this.agencia = agencia;
        this.conta = conta;
        this.dac = dac;
    }

    public Long getId() {
        return id;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public String getDac() {
        return dac;
    }

    public void setDac(String dac) {
        this.dac = dac;
    }

    @Override
    public String toString() {
        return "Conta{" +
                "id=" + id +
                ", agencia='" + agencia + '\'' +
                ", conta='" + conta + '\'' +
                ", dac='" + dac + '\'' +
                '}';
    }
}
