package com.jornada.next.tabajara;

import com.jornada.next.tabajara.model.Cliente;
import com.jornada.next.tabajara.model.Conta;
import com.jornada.next.tabajara.model.Endereco;
import com.jornada.next.tabajara.repository.ClienteRepository;
import com.jornada.next.tabajara.repository.ContaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.util.List;

@SpringBootApplication
public class TabajaraApplication {

    private static final Logger LOG = LoggerFactory.getLogger(TabajaraApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(TabajaraApplication.class, args);
    }

    @Bean
    public CommandLineRunner init(ClienteRepository clienteRepository, ContaRepository contaRepository) {
        return args -> {
            LOG.info("==========------INSERINDO DADOS EXEMPLOS------==========");
            var conta = new Conta("001", "123", "4");
            var endereco = new Endereco("Av. Paulista", "320", "", "São Paulo", "SP", "01310-000");
            var cliente = new Cliente("Bussunda","123.456.789-00","4000-4444","email@email.com", LocalDate.now(),endereco,conta);
            var cliente2 = new Cliente("Bussunda","123.456.789-00","4000-4444","email@email.com", LocalDate.now(),endereco,conta);

            contaRepository.save(conta);
            cliente.setConta(conta);
            cliente.setEndereco(endereco);
            cliente2.setConta(conta);
            cliente2.setEndereco(endereco);
            clienteRepository.saveAll(List.of(cliente,cliente2));

            LOG.info("==========------LISTA DE CLIENTES------==========");
            clienteRepository.findAll().forEach(System.out::println);
            LOG.info("==========------FIM------==========");


        };
    }
}
