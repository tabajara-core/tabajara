package com.jornada.next.tabajara.service.impl;

import com.jornada.next.tabajara.model.Cliente;
import com.jornada.next.tabajara.repository.ClienteRepository;
import com.jornada.next.tabajara.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    public List<Cliente> getAll() {
        return clienteRepository.findAll();
    }
}
