package com.jornada.next.tabajara.service;

import com.jornada.next.tabajara.model.Cliente;

import java.util.List;

public interface ClienteService {

    List<Cliente> getAll();
}
